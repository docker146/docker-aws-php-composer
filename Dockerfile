FROM docker:20

RUN apk add --no-cache curl \
					jq \
					python3 \
					py3-pip \
					git \
					php \
					php-curl \
					php-dom \
					php-fileinfo \
					php-iconv \
					php-json \
					php-mbstring \
					php-openssl \
					php-phar \
					php-simplexml \
					php-sockets \
					php-tokenizer \
					php-xml \
					php-xmlwriter

RUN pip3 install awscli

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer
